package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the manager database table.
 * 
 */
@Entity
@NamedQuery(name="Manager.findAll", query="SELECT m FROM Manager m")
public class Manager implements Serializable, IModel {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String email;

	private String name;

	private String phone;

	private String surname;

	//bi-directional many-to-one association to Tour
	@ManyToOne
	@JoinColumn(name="id_tour")
	private Tour tour;

	public Manager() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Tour getTour() {
		return this.tour;
	}

	public void setTour(Tour tour) {
		this.tour = tour;
	}
	@Override
	public String[] getTableHeaders() {
		return new String[]{"id","surname","name","email", "phone","id_tour","name","country","city","price",};
	}

	@Override
	public Object[] getTableRowData() {
		return new Object[]{id,surname,name,email,phone,tour.getId(),tour.getTourname(),tour.getCountry(),tour.getCity(),tour.getPrice()};
	}

	@Override
	public void updateWith(Object mask) {
		surname = ((Manager)mask).getSurname();
		name = ((Manager)mask).getName();
		email = ((Manager)mask).getEmail();
		phone = ((Manager)mask).getPhone();
		tour = ((Manager)mask).getTour();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Manager other = (Manager) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		return true;
	}

}