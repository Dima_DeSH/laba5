package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.text.SimpleDateFormat;



/**
 * The persistent class for the orders database table.
 * 
 */
@Entity
@Table(name="orders")
@NamedQuery(name="Order.findAll", query="SELECT o FROM Order o")
public class Order implements Serializable, IModel {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	//bi-directional many-to-one association to Client
	@ManyToOne
	@JoinColumn(name="id_client")
	private Client client;

	//bi-directional many-to-one association to Tour
	@ManyToOne
	@JoinColumn(name="id_tour")
	private Tour tour;

	public Order() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Tour getTour() {
		return this.tour;
	}

	public void setTour(Tour tour) {
		this.tour = tour;
	}
	@Override
	public String[] getTableHeaders() {
		return new String[] { "id", "date", "id_tour", "tourname", "city", "price", "country", "id_client",
				"surname", "name", "email", "phone", "passport" };
	}

	@Override
	public Object[] getTableRowData() {
		java.sql.Timestamp sqlDate = new java.sql.Timestamp(date.getTime());
		return new Object[] { id, sqlDate, tour.getId(), tour.getTourname(), tour.getCity(), tour.getPrice(),
			 tour.getCountry(), client.getId(), client.getSurname(), client.getName(),
				client.getEmail(), client.getPhone(), client.getPassport() };
	}

	@Override
	public void updateWith(Object mask) {
		date = ((Order) mask).getDate();
		tour = ((Order) mask).getTour();
		client = ((Order) mask).getClient();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((tour == null) ? 0 : tour.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (tour == null) {
			if (other.tour != null)
				return false;
		} else if (!tour.equals(other.tour))
			return false;
		return true;
	}

}