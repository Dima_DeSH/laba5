package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;
import static javax.persistence.GenerationType.IDENTITY;


/**
 * The persistent class for the tour database table.
 * 
 */
@Entity
@NamedQuery(name="Tour.findAll", query="SELECT t FROM Tour t")

public class Tour implements Serializable, IModel{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;

	private String city;

	private String country;

	private float price;

	private String tourname;

	//bi-directional many-to-one association to Manager
	@OneToMany(mappedBy="tour")
	private List<Manager> managers;

	//bi-directional many-to-one association to Order
	@OneToMany(mappedBy="tour")
	private List<Order> orders;

	public Tour() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public float getPrice() {
		return this.price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getTourname() {
		return this.tourname;
	}

	public void setTourname(String tourname) {
		this.tourname = tourname;
	}

	public List<Manager> getManagers() {
		return this.managers;
	}

	public void setManagers(List<Manager> managers) {
		this.managers = managers;
	}

	public Manager addManager(Manager manager) {
		getManagers().add(manager);
		manager.setTour(this);

		return manager;
	}

	public Manager removeManager(Manager manager) {
		getManagers().remove(manager);
		manager.setTour(null);

		return manager;
	}

	public List<Order> getOrders() {
		return this.orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public Order addOrder(Order order) {
		getOrders().add(order);
		order.setTour(this);

		return order;
	}

	public Order removeOrder(Order order) {
		getOrders().remove(order);
		order.setTour(null);

		return order;
	}
	@Override
	public String[] getTableHeaders() {
		return new String[]{"id","TourName","city","country","price"};
	}

	@Override
	public Object[] getTableRowData() {
		return new Object[]{id,tourname,city,country,price};
	}

	@Override
	public void updateWith(Object mask) {
		tourname = ((Tour)mask).getTourname();
		city = ((Tour)mask).getCity();
		price = ((Tour)mask).getPrice();
		country = ((Tour)mask).getCountry();	
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tourname == null) ? 0 : tourname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tour other = (Tour) obj;
		if (tourname == null) {
			if (other.tourname != null)
				return false;
		} else if (!tourname.equals(other.tourname))
			return false;
		if (city != other.city)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "��������:" + tourname + ", �����:" + city + ", ������:" + country 
				+ ", ����:" + price;
	}

}