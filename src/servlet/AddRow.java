package servlet;

import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.table.TableModel;
import controller.JpaController;
import controller.JpaControllerSelect;
import model.Client;
import model.Tour;
import model.IModel;
import model.Manager;
import model.Order;

@WebServlet("/AddRow")
public class AddRow extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public AddRow() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		JpaController controller = SelectTable.getController();
		String className = (String)request.getParameter("className");
		IModel obj = null;
		if(className == "")
			return;
		if(className.equals("Tour")){
			obj = new Tour();
			((Tour)obj).setTourname(request.getParameter("tourname"));
			
			
			((Tour)obj).setCity(request.getParameter("city"));
			((Tour)obj).setCountry(request.getParameter("country"));
			((Tour)obj).setPrice(Float.parseFloat(request.getParameter("price")));
		}
		else if(className.equals("Client")){
			obj = new Client();
			((Client)obj).setSurname(request.getParameter("surname"));
			((Client)obj).setName(request.getParameter("name"));
			((Client)obj).setEmail(request.getParameter("email"));
			((Client)obj).setPhone(request.getParameter("phone"));
			((Client)obj).setPassport(request.getParameter("passport"));
		}
		else if(className.equals("Order")){
			Tour tour = null;
			Client client = null;
			TableModel model = controller.getModel("Tour");
			int id_flat = Integer.parseInt(request.getParameter("selectTour"));
			int id_client = Integer.parseInt(request.getParameter("selectClient"));
			for(int i = 0; i < model.getRowCount(); i++){
				if((Integer)model.getValueAt(i, 0) == id_flat){
					tour = new Tour();
					tour.setId((Integer)model.getValueAt(i, 0));
					tour.setTourname((String)model.getValueAt(i, 1));
					tour.setCity((String)model.getValueAt(i, 2));
					tour.setCountry((String)model.getValueAt(i, 3));
					
					tour.setPrice((Float)model.getValueAt(i, 4));
				}
			}
			model = controller.getModel("Client");
			for(int i = 0; i < model.getRowCount(); i++){
				if((Integer)model.getValueAt(i, 0) == id_client){
					client = new Client();
					client.setId((Integer)model.getValueAt(i, 0));
					client.setSurname((String)model.getValueAt(i, 1));
					client.setName((String)model.getValueAt(i, 2));
					client.setEmail((String)model.getValueAt(i, 3));
					client.setPhone((String)model.getValueAt(i, 4));
					client.setPassport((String)model.getValueAt(i, 5));
				}
			}
			obj = new Order();
			((Order)obj).setClient(client);
			((Order)obj).setTour(tour);
			((Order)obj).setDate(new Date());
		}
		else if(className.equals("Manager")){
			Tour tour = null;
			TableModel model = controller.getModel("Tour");
			int id_flat = Integer.parseInt(request.getParameter("selectTour"));
			for(int i = 0; i < model.getRowCount(); i++){
				if((Integer)model.getValueAt(i, 0) == id_flat){
					tour = new Tour();
					tour.setId((Integer)model.getValueAt(i, 0));
					tour.setTourname((String)model.getValueAt(i, 1));
					tour.setCity((String)model.getValueAt(i, 2));
					tour.setCountry((String)model.getValueAt(i, 3));
					tour.setPrice((Float)model.getValueAt(i, 4));
				}
			}
			obj = new Manager();
			((Manager)obj).setSurname(request.getParameter("surname"));
			((Manager)obj).setName(request.getParameter("name"));
			((Manager)obj).setEmail(request.getParameter("email"));
			((Manager)obj).setPhone(request.getParameter("phone"));
			((Manager)obj).setTour(tour);
		}
		String id = request.getParameter("id");
		if(id != ""){
			request.setAttribute("className", className);
			request.setAttribute("id", id);
			request.setAttribute("obj", obj);
			request.getRequestDispatcher("UpdateRow").include(request, response);
		}
		else{
			controller.add(obj);
			request.setAttribute("className", className);
			request.setAttribute("act", "add");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
