package controller;

import java.util.List;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import model.Client;
import model.IModel;
import model.Manager;
import model.Order;
import model.Tour;
import controller.JpaController;

import model.IModel;

public class JpaControllerSelect implements IControllerSelect{
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("laba4_1");
	public List getObjectList(Class clazz) {
		EntityManager em = emf.createEntityManager();
		String queryName = clazz.getSimpleName() + "." + "findAll";
		List<Tour> list = em.createNamedQuery(queryName).getResultList();
		em.close();
		return list;
	}
	
	@Override
	public TableModel getModel(String className) {
		try {
			Class clazz = Class.forName("model." + className);
			IModel obj = (IModel) clazz.newInstance();
			String[] header = obj.getTableHeaders();
			List list = getObjectList(clazz);
			if (list == null || list.size() == 0)
				return new DefaultTableModel(null, header);
			Object[][] array = new Object[list.size()][header.length];
			int i = 0;
			for (Object s : list)
				array[i++] = ((IModel) s).getTableRowData();
			return new DefaultTableModel(array, header);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

�
}
