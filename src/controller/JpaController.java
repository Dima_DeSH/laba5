package controller;

import java.util.List;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import model.Client;
import model.IModel;
import model.Manager;
import model.Order;
import model.Tour;

public class JpaController implements IController {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("laba4_1");
	public List getObjectList(Class clazz) {
		EntityManager em = emf.createEntityManager();
		String queryName = clazz.getSimpleName() + "." + "findAll";
		List<Tour> list = em.createNamedQuery(queryName).getResultList();
		em.close();
		return list;
	}

	@Override
	public void createDB() {
		// TODO Auto-generated method stub

	}


	

	@Override
	public void add(Object obj) {
		Class clazz = obj.getClass();
		if(exist((IModel) obj)) return;
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(obj);
		em.getTransaction().commit();
	}

	@Override
	public void edit(int id, Object obj) {
		Class clazz = obj.getClass();
		if(exist((IModel) obj)) return;
		EntityManager em = emf.createEntityManager();
			em.getTransaction().begin();
			IModel model = (IModel) em.find(clazz, id);
			model.updateWith(obj); 
			em.getTransaction().commit();
	}

	@Override
	public void delete(int id, String className) {
		EntityManager em = emf.createEntityManager();
		try {
			Class clazz = Class.forName("model." + className);
			Object obj = em.find(clazz, id);
			em.getTransaction().begin();
			em.remove(obj);
			em.getTransaction().commit();
		} catch (ClassNotFoundException e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			em.close();
		}

	}

	public boolean exist(IModel obj) {
		Class clazz = obj.getClass();
		List list = getObjectList(clazz);
		if (list != null && list.size() != 0)
			for (Object current : list)
				if (current.equals(obj))
					return true;
		return false;
	}

	public TableModel doQuery() {
		EntityManager em = emf.createEntityManager();
		Query q = em.createQuery("SELECT f FROM Tour f WHERE f.price > ?1");
		q.setParameter(1, 500);
		List<Tour> list = q.getResultList();
		String[][] arr = new String[list.size()][6];
		System.out.println(list.size());
		int i = 0;
		for (Tour f : list) {
			arr[i][0] = String.valueOf(f.getId());
			arr[i][1] = String.valueOf(f.getTourname());
			arr[i][2] = String.valueOf(f.getCity());
			arr[i][3] = String.valueOf(f.getPrice());
			arr[i][5] = String.valueOf(f.getCountry());
			i++;
		}
		return new DefaultTableModel(arr, new String[] { "id", "tourName", "city", "price", "country" });
	}
}