package controller;

import javax.swing.table.TableModel;

public interface IController {
	void createDB();
	
	void add(Object obj);
	void edit(int id, Object obj);
	void delete(int id, String className);
	
}