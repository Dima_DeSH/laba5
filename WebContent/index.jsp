<%@page import="servlet.SelectTable"%>
<%@page import="controller.JpaController"%>
<%@page import="java.util.Formatter"%>
<%@page import="javax.swing.table.TableModel"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="org.apache.jasper.runtime.JspWriterImpl"%>
<%@page import="java.io.Writer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%!
		JpaController controller = SelectTable.getController();
		TableModel model;
		
		public String showTour(String id_flat){
			StringBuilder sb = new StringBuilder();
			model = controller.getModel("Tour");
			for (int i = 0; i < model.getRowCount(); i++) {
				String line = String.format("|%-3d|%-30s|%-20s|%-20s|%-12.2f|", model.getValueAt(i, 0),
			model.getValueAt(i, 1), model.getValueAt(i, 2), model.getValueAt(i, 3), model.getValueAt(i, 4));
				line = line.replaceAll(" ", "&nbsp;");
				if(id_flat != null){
					if(id_flat.equals(String.valueOf(model.getValueAt(i, 0))))
						sb.append("<option selected value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");
					else
						sb.append("<option value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");			
				}
				else{
					sb.append("<option value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");
				}
			}
			return sb.toString();
		}
		
		public String showClient(String id_client){
			StringBuilder sb = new StringBuilder();
			model = controller.getModel("Client");
			for (int i = 0; i < model.getRowCount(); i++) {
				String line = String.format("|%-3d|%-12s|%-10s|%-20s|%-13s|%-9s|", model.getValueAt(i, 0),model.getValueAt(i, 1), model.getValueAt(i, 2), model.getValueAt(i, 3), model.getValueAt(i, 4),model.getValueAt(i, 5));
				line = line.replaceAll(" ", "&nbsp;");
				if(id_client != null){
					if(id_client.equals(String.valueOf(model.getValueAt(i, 0))))
						sb.append("<option selected value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");
					else
						sb.append("<option value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");			
				}
				else{
					sb.append("<option value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");
				}
			}
			return sb.toString();
		}
	%>
	<% 
		String table = (String)request.getAttribute("table");
		String className = (String)request.getAttribute("className");
		String act = (String)request.getAttribute("act");
		String str = (String)request.getAttribute("str");
		String id_flat = (String)request.getAttribute("id_flat");
		String id_client = (String)request.getAttribute("id_client");
	%>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Практика</title>
	<link rel="stylesheet" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
	<%
		if (act != null && className != null) {
			if (act.equals("add"))
				out.write("document.getElementById('select" + className + "').click();");
			else if (act.equals("delete"))
				out.write("document.getElementById('delete" + className + "').click();");
			else if (act.equals("update"))
				out.write("document.getElementById('change" + className + "').click();");
	}%>
	$("#flatBlock,#clientBlock,#orderBlock,#managerBlock").dialog({autoOpen:false, modal:true, resizable:false, show:'fade', draggable : false});
	$("#flatBlock").dialog({title:'Тур',width:400});
	$("#clientBlock").dialog({title:'Клиент',width:400});
	$("#orderBlock").dialog({title:'Заказ',width:650});
	$("#managerBlock").dialog({title:'Менеджер',width:650});
						$("#addTab a").click(function(event) {
							block = $(this).attr('href');
							$(block).dialog('open');	
						});
						$("#add,#edit,#delete").mouseenter(function(event) {
							var id = "#" + $(this).attr('id');
							$(id).css("color", "yellowgreen");
							$(id).css("background-color", "black");
							var s = id + "Tab";
							$(s).slideDown(200, function() {
								$(s).css("height", "30px");
							});
						});
						$("#add,#edit,#delete").mouseleave(function() {
							var id = "#" + $(this).attr('id');
							var s = id + "Tab";
							if (!$(s).is(":hover")) {
								$(s).slideUp(200);
								$(id).css("background-color", "#222");
								$(id).css("color", "#9d9d9d");
							}
						});
						$("#addTab,#editTab,#deleteTab").mouseleave(function() {
							var id = "#" + $(this).attr('id');
							var s = id.substr(0, id.length - 3);
							if (!$(s).is(":hover")) {
								$(id).slideUp(200);
								$(s).css("background-color", "#222");
								$(s).css("color", "#9d9d9d");
							}
						});
						<%
						if (act != null && className != null) {
								if(str != null){
									out.write(str);
								}
						}%>
					});
	</script>
</head>
<body>
	<header>
		<h1>БД "Туристическое агенство"</h1>
	</header>
	<nav class="page-navigation">
		<div class="container">
			<ul>
				<li><a id="add" href="#">Добавить</a></li>
				<li><a id="edit" href="#">Редактировать</a></li>
				<li><a id="delete" href="#">Удалить</a></li>
				<li><a href="SpecialQuery">Запрос</a></li>
			</ul>
		</div>
	</nav>
	<div id="addTab" class="page-navigationOne" hidden>
		<div class="container">
			<ul>
				<li><a href="#flatBlock">Тур</a></li>
				<li><a href="#orderBlock">Заказ</a></li>
				<li><a href="#clientBlock">Клиент</a></li>
				<li><a href="#managerBlock">Менеджер</a></li>
			</ul>
		</div>
	</div>
	<div id="editTab" class="page-navigationOne" hidden>
		<div class="container">
			<ul>
				<li><a id="changeTour"
					href="SelectTable?className=Tour&act=ChangeAction">Тур</a></li>
				<li><a id="changeOrder"
					href="SelectTable?className=Order&act=ChangeAction">Заказ</a></li>
				<li><a id="changeClient"
					href="SelectTable?className=Client&act=ChangeAction">Клиент</a></li>
				<li><a id="changeManager"
					href="SelectTable?className=Manager&act=ChangeAction">Менеджер</a></li>
			</ul>
		</div>
	</div>
	<div id="deleteTab" class="page-navigationOne" hidden>
		<div class="container">
			<ul>
				<li><a id="deleteTour"
					href="SelectTable?className=Tour&act=DeleteAction">Тур</a></li>
				<li><a id="deleteOrder"
					href="SelectTable?className=Order&act=DeleteAction">Заказ</a></li>
				<li><a id="deleteClient"
					href="SelectTable?className=Client&act=DeleteAction">Клиент</a></li>
				<li><a id="deleteManager"
					href="SelectTable?className=Manager&act=DeleteAction">Менеджер</a></li>
			</ul>
		</div>
	</div>
	<div id="flatBlock">
		<form action="AddRow" method="GET">
			<input type="hidden" name="id"> 
			<div>Название<input type="text" name="tourname"></div>
			<div>Город<input type="text" name="city"></div>
			<div>Страна<input type="text" name="country"></div>
			<div>Цена<input type="text" name="price"></div>
			<input type="hidden" name="className" value="Tour"> 
			<input name="accept" type="submit" value="OK">
		</form>
	</div>
	<div id="clientBlock">
		<form action="AddRow" method="GET">
			<input type="hidden" name="id"> 
			<div>Фамилия<input type="text" name="surname"></div>
			<div>Имя<input type="text" name="name"></div>
			<div>Ел.пошта<input type="text" name="email"></div>
			<div>Телефон<input type="text" name="phone"></div>
			<div>Номер пасспорта<input type="text" name="passport"></div>
			<input type="hidden" name="className" value="Client"> 
			<input name="accept" type="submit" value="OK">
		</form>
	</div>
	<div id="orderBlock">
		<form action="AddRow" method="GET">
			<input type="hidden" name="id"> 
			<p>Выберите тур</p>
			<pre>  ID               Название              Город  Страна        Цена</pre>
			<select name="selectTour" style="font-family: monospace">
				<% out.write(showTour(id_flat));%>
			</select>
			<p>Выберите клиента</p>
			<pre>  ID   Фамилия      Имя         Ел.пошта         Телефон      Пасспорт</pre>
			<select name="selectClient" style="font-family: monospace">
				<% out.write(showClient(id_client));%>
			</select> 
			<input type="hidden" name="className" value="Order">
			<input class="Ok" name="accept" type="submit" value="OK">
		</form>
	</div>
	<div id="managerBlock">
		<form action="AddRow" method="GET">
			<input type="hidden" name="id"> 
			<div>Фамилия<input type="text" name="surname"></div>
			<div>Имя<input type="text" name="name"></div>
			<div>Ел.пошта<input type="text" name="email"></div>
			<div>Телефон<input type="text" name="phone"></div>
			<p>Выберите </p>
			<pre>  ID               Адресс               Город   Страна         Ціна</pre>
			<select name="selectTour" style="font-family: monospace">
				<% out.write(showTour(id_flat));%>
			</select> 
			<input type="hidden" name="className" value="Manager"> 
			<input class="Ok" name="accept" type="submit" value="OK">
		</form>
	</div>
	<div id="block">
		<div class="choose">
			<div>
				<ul>
					Выберите таблицу:
					<li><a id="selectTour" href="SelectTable?className=Tour&act=AddAction">Туры</a></li>
					<li><a id="selectOrder" href="SelectTable?className=Order&act=AddAction">Заказы</a></li>
					<li><a id="selectClient" href="SelectTable?className=Client&act=AddAction">Клиенты</a></li>
					<li><a id="selectManager" href="SelectTable?className=Manager&act=AddAction">Менеджеры</a></li>
				</ul>
			</div>
		</div>
		<div id="table">
			<%if(table != null)
				out.write(table.toString());
			%>
		</div>
	</div>
	<footer> Pomoyka-TEAM</br></footer>
</body>
</html>