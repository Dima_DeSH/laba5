<%@page import="servlet.SelectTable"%>
<%@page import="controller.JpaController"%>
<%@page import="java.util.Formatter"%>
<%@page import="javax.swing.table.TableModel"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="org.apache.jasper.runtime.JspWriterImpl"%>
<%@page import="java.io.Writer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%!
JpaController controller = SelectTable.getController();
TableModel model; 
public String showClient(String id_client){
			StringBuilder sb = new StringBuilder();
			model = controller.getModel("Client");
			for (int i = 0; i < model.getRowCount(); i++) {
				String line = String.format("|%-3d|%-12s|%-10s|%-20s|%-13s|%-9s|", model.getValueAt(i, 0),model.getValueAt(i, 1), model.getValueAt(i, 2), model.getValueAt(i, 3), model.getValueAt(i, 4),model.getValueAt(i, 5));
				line = line.replaceAll(" ", "&nbsp;");
				if(id_client != null){
					if(id_client.equals(String.valueOf(model.getValueAt(i, 0))))
						sb.append("<option selected value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");
					else
						sb.append("<option value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");			
				}
				else{
					sb.append("<option value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");
				}
			}
			return sb.toString();
		}
public String showTour(String id_flat){
	StringBuilder sb = new StringBuilder();
	model = controller.getModel("Tour");
	for (int i = 0; i < model.getRowCount(); i++) {
		String line = String.format("|%-3d|%-30s|%-20s|%-20s|%-12.2f|", model.getValueAt(i, 0),
	model.getValueAt(i, 1), model.getValueAt(i, 2), model.getValueAt(i, 3), model.getValueAt(i, 4));
		line = line.replaceAll(" ", "&nbsp;");
		if(id_flat != null){
			if(id_flat.equals(String.valueOf(model.getValueAt(i, 0))))
				sb.append("<option selected value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");
			else
				sb.append("<option value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");			
		}
		else{
			sb.append("<option value="+model.getValueAt(i, 0)+"><tt>"+line+"</tt></option>");
		}
	}
	return sb.toString();
}
	%>
</body>
</html>